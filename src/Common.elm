module Common exposing (..)

import Date exposing (Date)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Html.Attributes
import Html.Events
import Http exposing (..)
import Json.Decode exposing (..)
import Json.Encode as Encode exposing (Value, object)
import Time exposing (..)


type alias IdNombre =
    { id : Int
    , nombre : String
    }


type alias IdNombres =
    List IdNombre


type alias LoginData =
    { userId : Int
    , token : String
    }


stringToMaybeInt : String -> Maybe Int -> Maybe Int
stringToMaybeInt s old =
    if String.isEmpty s then
        Nothing

    else
        case String.toInt s of
            Nothing ->
                old

            Just i ->
                Just i


maybeIntToString : Maybe Int -> String
maybeIntToString mi =
    case mi of
        Just i ->
            String.fromInt i

        Nothing ->
            ""


getNombre : Maybe IdNombre -> String
getNombre mIdNombre =
    case mIdNombre of
        Just idNombre ->
            idNombre.nombre

        Nothing ->
            "No se pudo cargar"


idNombreDecoder : Decoder IdNombre
idNombreDecoder =
    map2 IdNombre
        (field "id" int)
        (field "nombre" string)


idNombresDecoder : Decoder IdNombres
idNombresDecoder =
    list idNombreDecoder


post :
    { url : String
    , token : String
    , body : Body
    , expect : Expect msg
    }
    -> Cmd msg
post r =
    request
        { method = "POST"
        , headers = [ header "Authorization" ("Bearer " ++ r.token) ]
        , url = r.url
        , body = r.body
        , expect = r.expect
        , timeout = Nothing
        , tracker = Nothing
        }


get :
    { url : String
    , token : String
    , expect : Expect msg
    }
    -> Cmd msg
get r =
    request
        { method = "GET"
        , headers = [ header "Authorization" ("Bearer " ++ r.token) ]
        , url = r.url
        , body = emptyBody
        , expect = r.expect
        , timeout = Nothing
        , tracker = Nothing
        }


idDecoder : Decoder Int
idDecoder =
    field "id" int


encodeMaybeIdNombre : Maybe IdNombre -> Value
encodeMaybeIdNombre mval =
    case mval of
        Just val ->
            Encode.int val.id

        Nothing ->
            Encode.null


encodeMaybeIntString : Maybe String -> Value
encodeMaybeIntString mval =
    case mval of
        Just val ->
            case String.toInt val of
                Just n ->
                    Encode.int n

                Nothing ->
                    Encode.null

        Nothing ->
            Encode.null


encodeMaybeInt : Maybe Int -> Value
encodeMaybeInt mval =
    case mval of
        Just val ->
            Encode.int val

        Nothing ->
            Encode.null


encodeMaybeDate : Maybe Date -> Value
encodeMaybeDate mdate =
    case mdate of
        Just date ->
            Encode.string (Date.toIsoString date)

        Nothing ->
            Encode.null


textStyleOptional : Maybe IdNombre -> Attr decorative msg
textStyleOptional mIdNombre =
    case mIdNombre of
        Nothing ->
            Font.color (Element.rgb 1 0 0)

        Just _ ->
            Font.color (Element.rgb 0 0 0)


textAlignRight : List (Attribute msg)
textAlignRight =
    [ Font.alignRight
    , htmlAttribute <| Html.Attributes.style "text-align" "inherit"
    ]


onEnter : msg -> Element.Attribute msg
onEnter msg =
    Element.htmlAttribute
        (Html.Events.on "keyup"
            (field "key" string
                |> andThen
                    (\key ->
                        if key == "Enter" then
                            succeed msg

                        else
                            fail "Not the enter key"
                    )
            )
        )


esMonth : Date.Month -> String
esMonth m =
    case m of
        Jan ->
            "Enero"

        Feb ->
            "Febrero"

        Mar ->
            "Marzo"

        Apr ->
            "Abril"

        May ->
            "Mayo"

        Jun ->
            "Junio"

        Jul ->
            "Julio"

        Aug ->
            "Agosto"

        Sep ->
            "Septiembre"

        Oct ->
            "Octubre"

        Nov ->
            "Noviembre"

        Dec ->
            "Diciembre"


esMonthShort : Date.Month -> String
esMonthShort m =
    case m of
        Jan ->
            "Ene"

        Feb ->
            "Feb"

        Mar ->
            "Mar"

        Apr ->
            "Abr"

        May ->
            "May"

        Jun ->
            "Jun"

        Jul ->
            "Jul"

        Aug ->
            "Ago"

        Sep ->
            "Sep"

        Oct ->
            "Oct"

        Nov ->
            "Nov"

        Dec ->
            "Dic"


esWeekday : Date.Weekday -> String
esWeekday w =
    case w of
        Mon ->
            "Lunes"

        Tue ->
            "Martes"

        Wed ->
            "Miercoles"

        Thu ->
            "Jueves"

        Fri ->
            "Viernes"

        Sat ->
            "Sabado"

        Sun ->
            "Domingo"


esWeekdayShort : Date.Weekday -> String
esWeekdayShort w =
    case w of
        Mon ->
            "Lun"

        Tue ->
            "Mar"

        Wed ->
            "Mie"

        Thu ->
            "Jue"

        Fri ->
            "Vie"

        Sat ->
            "Sab"

        Sun ->
            "Dom"


esDay : Int -> String
esDay n =
    ""


es : Date.Language
es =
    { monthName = esMonth
    , monthNameShort = esMonthShort
    , weekdayName = esWeekday
    , weekdayNameShort = esWeekdayShort
    , dayWithSuffix = esDay
    }
