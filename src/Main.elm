module Main exposing (..)

import Browser
import Browser.Navigation as Nav
import Common exposing (..)
import Date exposing (Date)
import Dropdown
import Egreso as E
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import File.Download as Download
import Html exposing (Html)
import Html.Attributes as Attributes
import Http exposing (..)
import Ingreso as I
import Json.Decode as Decode exposing (Decoder, decodeString, field, float, int, list, map2, map3, map4, maybe, string)
import Json.Decode.Pipeline exposing (hardcoded, optional, required)
import Json.Encode as Encode exposing (Value, object)
import Login
import Route exposing (Route, fromUrl)
import Url


type Page
    = PLogin Login.Model
    | PHome
    | PEgreso E.Model
    | PIngreso I.Model
    | PNotFound


type alias Ingreso =
    { userId : Int
    , tipoDocumentoId : Maybe Int
    , condicionVentaId : Maybe Int
    , tipoIdentificacionId : Maybe Int
    , tipoIngresoId : Maybe Int
    , fecha : String
    , numeroTimbrado : String
    , numeroDocumento : String
    , numeroIdentificacion : String
    , gravado : Maybe Int
    , noGravado : Maybe Int
    , nombre : String
    }


type alias Ingresos =
    List Ingreso


type alias Egreso =
    { userId : Int
    , categoriaId : Maybe Int
    , tipoDocumentoId : Maybe Int
    , condicionVentaId : Maybe Int
    , tipoIdentificacionId : Maybe Int
    , tipoEgresoId : Maybe Int
    , clasificacionEgresoId : Maybe Int
    , fecha : String
    , numeroTimbrado : String
    , numeroDocumento : String
    , numeroIdentificacion : String
    , iva10 : Maybe Int
    , iva5 : Maybe Int
    , exentas : Maybe Int
    , nombre : String
    , monto : Int
    }


type alias Egresos =
    List Egreso


type alias Periodos =
    { periodos : List Int }


type alias Model =
    { key : Nav.Key
    , url : Url.Url
    , page : Page
    , apiUrl : String
    , loginData : LoginData
    , periodo : Maybe String
    , periodos : List String
    , egresos : Egresos
    , ingresos : Ingresos
    }


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | EgresoMsg E.Msg
    | IngresoMsg I.Msg
    | LoginMsg Login.Msg
    | Export
    | PeriodoChanged (Maybe String)
    | GotAranduka (Result Http.Error String)
    | GotEgresos (Result Http.Error Egresos)
    | GotIngresos (Result Http.Error Ingresos)
    | GotPeriodos (Result Http.Error (List Int))
    | NoAction


main : Program String Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }


ingresoDecoder : Decoder Ingreso
ingresoDecoder =
    Decode.succeed Ingreso
        |> required "userId" int
        |> required "tipoDocumentoId" (maybe int)
        |> required "condicionVentaId" (maybe int)
        |> required "tipoIdentificacionId" (maybe int)
        |> required "tipoIngresoId" (maybe int)
        |> required "fecha" string
        |> required "numeroTimbrado" string
        |> required "numeroDocumento" string
        |> required "numeroIdentificacion" string
        |> required "gravado" (maybe int)
        |> required "noGravado" (maybe int)
        |> required "nombre" string


ingresosDecoder : Decoder Ingresos
ingresosDecoder =
    list ingresoDecoder


egresoDecoder : Decoder Egreso
egresoDecoder =
    Decode.succeed Egreso
        |> required "userId" int
        |> required "categoriaId" (maybe int)
        |> required "tipoDocumentoId" (maybe int)
        |> required "condicionVentaId" (maybe int)
        |> required "tipoIdentificacionId" (maybe int)
        |> required "tipoEgresoId" (maybe int)
        |> required "clasificacionEgresoId" (maybe int)
        |> required "fecha" string
        |> required "numeroTimbrado" string
        |> required "numeroDocumento" string
        |> required "numeroIdentificacion" string
        |> required "iva10" (maybe int)
        |> required "iva5" (maybe int)
        |> required "exentas" (maybe int)
        |> required "nombre" string
        |> required "monto" int


egresosDecoder : Decoder Egresos
egresosDecoder =
    list egresoDecoder


periodosDecoder : Decoder (List Int)
periodosDecoder =
    field "periodos" (list int)


getIngresos : String -> String -> Int -> Cmd Msg
getIngresos apiUrl token limit =
    Common.get
        { url = apiUrl ++ "ingresos" ++ "?limit=" ++ String.fromInt limit
        , token = token
        , expect = Http.expectJson GotIngresos ingresosDecoder
        }


getEgresos : String -> String -> Int -> Cmd Msg
getEgresos apiUrl token limit =
    Common.get
        { url = apiUrl ++ "egresos" ++ "?limit=" ++ String.fromInt limit
        , token = token
        , expect = Http.expectJson GotEgresos egresosDecoder
        }


getPeriodos : String -> String -> Cmd Msg
getPeriodos apiUrl token =
    Common.get
        { url = apiUrl ++ "periodos"
        , token = token
        , expect = Http.expectJson GotPeriodos periodosDecoder
        }


load : String -> String -> Cmd Msg
load apiUrl token =
    Cmd.batch
        [ getIngresos apiUrl token 5
        , getEgresos apiUrl token 5
        , getPeriodos apiUrl token
        ]


init : String -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init apiUrl url key =
    ( { key = key
      , url = url
      , page = PLogin <| Login.init apiUrl
      , apiUrl = apiUrl
      , loginData = { userId = 0, token = "" }
      , periodo = Nothing
      , periodos = []
      , egresos = []
      , ingresos = []
      }
    , Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


changeRouteTo : Model -> Maybe Route -> ( Model, Cmd Msg )
changeRouteTo model maybeRoute =
    if String.isEmpty model.loginData.token then
        ( { model | page = PLogin <| Login.init model.apiUrl }
        , Cmd.none
        )

    else
        case maybeRoute of
            Nothing ->
                ( { model | page = PNotFound }, Cmd.none )

            Just Route.Login ->
                ( { model
                    | page =
                        PLogin <|
                            Login.init model.apiUrl
                  }
                , Cmd.none
                )

            Just Route.Home ->
                ( { model | page = PHome }
                , load model.apiUrl model.loginData.token
                )

            Just Route.Egreso ->
                ( { model
                    | page =
                        PEgreso <|
                            E.init model.apiUrl model.loginData
                  }
                , Cmd.map (\m -> EgresoMsg m)
                    (E.load model.apiUrl model.loginData.token)
                )

            Just Route.Ingreso ->
                ( { model
                    | page =
                        PIngreso <|
                            I.init model.apiUrl model.loginData
                  }
                , Cmd.map (\m -> IngresoMsg m)
                    (I.load model.apiUrl model.loginData.token)
                )

            Just Route.NotFound ->
                ( { model | page = PNotFound }, Cmd.none )


getHomeUrl : Url.Url -> String
getHomeUrl url =
    let
        currentUrl =
            Url.toString url

        endPos =
            Maybe.withDefault 0 (List.head (String.indexes "/#" currentUrl))
    in
    String.slice 0 endPos currentUrl


gotoHome : Model -> Cmd Msg
gotoHome model =
    Nav.pushUrl model.key (getHomeUrl model.url)


getAranduka : String -> String -> Maybe String -> Cmd Msg
getAranduka apiUrl token mperiodo =
    case mperiodo of
        Just periodo ->
            Common.get
                { url = apiUrl ++ "aranduka/" ++ periodo
                , token = token
                , expect = Http.expectString GotAranduka
                }

        Nothing ->
            Cmd.none


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            changeRouteTo { model | url = url } (Route.fromUrl url)

        EgresoMsg egresoMsg ->
            case model.page of
                PEgreso egreso ->
                    let
                        ( newEgreso, answer ) =
                            E.update egresoMsg egreso
                    in
                    case answer of
                        E.Close ->
                            ( { model | page = PHome }
                            , gotoHome model
                            )

                        E.SendCmd cmd ->
                            ( { model | page = PEgreso newEgreso }
                            , Cmd.map (\m -> EgresoMsg m) cmd
                            )

                        E.NoAnswer ->
                            ( { model | page = PEgreso newEgreso }
                            , Cmd.none
                            )

                _ ->
                    ( model, Cmd.none )

        IngresoMsg ingresoMsg ->
            case model.page of
                PIngreso ingreso ->
                    let
                        ( newIngreso, answer ) =
                            I.update ingresoMsg ingreso
                    in
                    case answer of
                        I.Close ->
                            ( { model | page = PHome }
                            , gotoHome model
                            )

                        I.SendCmd cmd ->
                            ( { model | page = PIngreso newIngreso }
                            , Cmd.map (\m -> IngresoMsg m) cmd
                            )

                        I.NoAnswer ->
                            ( { model | page = PIngreso newIngreso }
                            , Cmd.none
                            )

                _ ->
                    ( model, Cmd.none )

        LoginMsg loginMsg ->
            case model.page of
                PLogin login ->
                    let
                        ( newLogin, answer ) =
                            Login.update loginMsg login
                    in
                    case answer of
                        Login.NoAnswer ->
                            ( { model | page = PLogin newLogin }
                            , Cmd.none
                            )

                        Login.LoggedIn data ->
                            ( { model | page = PHome, loginData = data }
                            , load model.apiUrl data.token
                            )

                        Login.SendCmd cmd ->
                            ( { model | page = PLogin newLogin }
                            , Cmd.map (\m -> LoginMsg m) cmd
                            )

                _ ->
                    ( model, Cmd.none )

        Export ->
            ( model
            , getAranduka
                model.apiUrl
                model.loginData.token
                model.periodo
            )

        PeriodoChanged periodo ->
            ( { model | periodo = periodo }, Cmd.none )

        GotAranduka result ->
            case result of
                Ok data ->
                    ( model
                    , Download.string
                        "response.json"
                        "application/json"
                        data
                    )

                Err err ->
                    ( model, Cmd.none )

        GotEgresos result ->
            case result of
                Ok data ->
                    ( { model | egresos = data }
                    , Cmd.none
                    )

                Err err ->
                    ( model, Cmd.none )

        GotIngresos result ->
            case result of
                Ok data ->
                    ( { model | ingresos = data }
                    , Cmd.none
                    )

                Err err ->
                    ( model, Cmd.none )

        GotPeriodos result ->
            case result of
                Ok data ->
                    let
                        periodos =
                            List.map String.fromInt data

                        periodo =
                            List.head periodos
                    in
                    ( { model | periodos = periodos, periodo = periodo }
                    , Cmd.none
                    )

                Err err ->
                    ( model, Cmd.none )

        NoAction ->
            ( model, Cmd.none )


view : Model -> Browser.Document Msg
view model =
    { title = "Finanzas"
    , body = [ viewBody model ]
    }


viewBody : Model -> Html Msg
viewBody model =
    Element.layout [] (viewMain model)


button : Element Msg -> Element Msg
button e =
    el
        [ Border.color (rgb 0 0 0)
        , Border.width 2
        , Border.rounded 3
        , padding 5
        ]
        e


viewMain : Model -> Element Msg
viewMain model =
    case model.page of
        PLogin login ->
            Element.map (\lmsg -> LoginMsg lmsg) (Login.view login)

        PHome ->
            viewHome model

        PEgreso egreso ->
            Element.map (\emsg -> EgresoMsg emsg) (E.view egreso)

        PIngreso ingreso ->
            Element.map (\imsg -> IngresoMsg imsg) (I.view ingreso)

        PNotFound ->
            Element.text "Page not found"


dropdownOptions : List String -> Dropdown.Options Msg
dropdownOptions values =
    let
        defaultOptions =
            Dropdown.defaultOptions PeriodoChanged
    in
    { defaultOptions
        | items =
            List.map
                (\v ->
                    { value = v
                    , text = v
                    , enabled = True
                    }
                )
                values
        , emptyItem = Nothing -- Just { value = "", text = "[Seleccione por favor]", enabled = True }
    }


viewHome : Model -> Element Msg
viewHome model =
    column [ width fill, padding 5, spacing 10 ]
        [ row [ spacing 10 ]
            [ Element.link []
                { url = "#/login/"
                , label = button (Element.text "Login")
                }
            , Element.link []
                { url = "#/egreso/"
                , label = button (Element.text "Egreso")
                }
            , Element.link []
                { url = "#/ingreso/"
                , label = button (Element.text "Ingreso")
                }
            ]
        , row []
            [ Element.text "Periodo"
            , el [ width shrink ] <|
                html <|
                    Dropdown.dropdown
                        (dropdownOptions model.periodos)
                        []
                        model.periodo
            , Input.button []
                { onPress = Just Export
                , label = Element.text "Exportar"
                }
            ]
        , viewIngresos model
        , viewEgresos model
        ]


title : String -> Element Msg
title text =
    el [ Font.size 22, Font.bold ] <| Element.text text


tableHeaderString : String -> Element Msg
tableHeaderString text =
    el [ Font.size 20, Font.bold ] <| Element.text text


tableHeaderNumber : String -> Element Msg
tableHeaderNumber text =
    el [ Font.size 20, Font.bold, Font.alignRight ] <| Element.text text


tableEntryString : String -> Element Msg
tableEntryString text =
    el [ Font.size 18 ] <| Element.text text


tableEntryNumber : String -> Element Msg
tableEntryNumber text =
    el [ Font.size 18, Font.alignRight ] <| Element.text text


viewEgresos : Model -> Element Msg
viewEgresos model =
    column [ spacing 7 ]
        [ title "Egresos"
        , Element.table [ spacingXY 20 3 ]
            { data = model.egresos
            , columns =
                [ { header = tableHeaderString "fecha"
                  , width = shrink
                  , view = \egreso -> tableEntryString egreso.fecha
                  }
                , { header = tableHeaderNumber "monto"
                  , width = shrink
                  , view =
                        \egreso ->
                            tableEntryNumber <|
                                String.fromInt egreso.monto
                  }
                , { header = tableHeaderString "nombre"
                  , width = fill
                  , view = \egreso -> tableEntryString egreso.nombre
                  }
                ]
            }
        ]


viewIngresos : Model -> Element Msg
viewIngresos model =
    column [ spacing 7 ]
        [ title "Ingresos"
        , Element.table [ spacingXY 20 3 ]
            { data = model.ingresos
            , columns =
                [ { header = tableHeaderString "fecha"
                  , width = shrink
                  , view = \ingreso -> tableEntryString ingreso.fecha
                  }
                , { header = tableHeaderString "Gravado"
                  , width = shrink
                  , view =
                        \ingreso ->
                            tableEntryNumber <|
                                maybeIntToString ingreso.gravado
                  }
                , { header = tableHeaderString "No Gravado"
                  , width = shrink
                  , view =
                        \ingreso ->
                            tableEntryNumber <|
                                maybeIntToString ingreso.noGravado
                  }
                , { header = tableHeaderString "nombre"
                  , width = fill
                  , view = \ingreso -> tableEntryString ingreso.nombre
                  }
                ]
            }
        ]
