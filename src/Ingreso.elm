module Ingreso exposing (..)

import Browser
import Browser.Navigation as Nav
import Common exposing (..)
import Date exposing (Date)
import DatePicker exposing (ChangeEvent(..))
import Dropdown
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Html exposing (Html)
import Html.Attributes
import Http exposing (..)
import Json.Decode exposing (Decoder, decodeString, field, float, int, list, map2, map3, map4, maybe, string)
import Json.Encode as Encode exposing (Value, object)
import Maybe.Extra
import Route exposing (Route, fromUrl)
import Task
import Url


type Answer
    = Close
    | SendCmd (Cmd Msg)
    | NoAnswer


type alias Ingreso =
    { tipoDocumento : Maybe String
    , condicionVenta : Maybe String
    , tipoIdentificacion : Maybe String
    , tipoIngreso : Maybe String
    , fecha : Maybe Date
    , numeroTimbrado : String
    , numeroDocumento : String
    , numeroIdentificacion : String
    , gravado : Maybe Int
    , noGravado : Maybe Int
    , nombre : String
    }


type alias Model =
    { tiposDocumento : IdNombres
    , condicionesVenta : IdNombres
    , tiposIdentificacion : IdNombres
    , tiposIngreso : IdNombres
    , ingreso : Ingreso
    , dateText : String
    , pickerModel : DatePicker.Model
    , apiUrl : String
    , loginData : LoginData
    }


type Msg
    = ChangePicker ChangeEvent
    | SetToday Date
    | GotTiposDocumento (Result Http.Error IdNombres)
    | GotCondicionesVenta (Result Http.Error IdNombres)
    | GotTiposIdentificacion (Result Http.Error IdNombres)
    | GotTiposIngreso (Result Http.Error IdNombres)
    | NumeroTimbradoChanged String
    | NumeroDocumentoChanged String
    | NumeroIdentificacionChanged String
    | GravadoChanged String
    | NoGravadoChanged String
    | NombreChanged String
    | CancelNewIngreso
    | OkNewIngreso
    | GotNewIngreso (Result Http.Error Int)
    | TipoDocumentoChanged (Maybe String)
    | CondicionVentaChanged (Maybe String)
    | TipoIdentificacionChanged (Maybe String)
    | TipoIngresoChanged (Maybe String)
    | NoAction


initIngreso : Ingreso
initIngreso =
    { tipoDocumento = Nothing
    , condicionVenta = Nothing
    , tipoIdentificacion = Nothing
    , tipoIngreso = Nothing
    , fecha = Nothing
    , numeroTimbrado = ""
    , numeroDocumento = ""
    , numeroIdentificacion = ""
    , gravado = Nothing
    , noGravado = Nothing
    , nombre = ""
    }


init : String -> LoginData -> Model
init apiUrl loginData =
    { tiposDocumento = []
    , condicionesVenta = []
    , tiposIdentificacion = []
    , tiposIngreso = []
    , ingreso = initIngreso
    , dateText = ""
    , pickerModel = DatePicker.init
    , apiUrl = apiUrl
    , loginData = loginData
    }


getTiposDocumento : String -> String -> Cmd Msg
getTiposDocumento apiUrl token =
    Common.get
        { url = apiUrl ++ "tipos-documento-ingreso"
        , token = token
        , expect = Http.expectJson GotTiposDocumento idNombresDecoder
        }


getCondicionesVenta : String -> String -> Cmd Msg
getCondicionesVenta apiUrl token =
    Common.get
        { url = apiUrl ++ "condiciones-venta"
        , token = token
        , expect = Http.expectJson GotCondicionesVenta idNombresDecoder
        }


getTiposIdentificacion : String -> String -> Cmd Msg
getTiposIdentificacion apiUrl token =
    Common.get
        { url = apiUrl ++ "tipos-identificacion"
        , token = token
        , expect = Http.expectJson GotTiposIdentificacion idNombresDecoder
        }


getTiposIngreso : String -> String -> Cmd Msg
getTiposIngreso apiUrl token =
    Common.get
        { url = apiUrl ++ "tipos-ingreso"
        , token = token
        , expect = Http.expectJson GotTiposIngreso idNombresDecoder
        }


load : String -> String -> Cmd Msg
load apiUrl token =
    Cmd.batch
        [ getTiposDocumento apiUrl token
        , getCondicionesVenta apiUrl token
        , getTiposIdentificacion apiUrl token
        , getTiposIngreso apiUrl token
        , Task.perform SetToday Date.today
        ]


encodeNewIngreso : Int -> Ingreso -> Value
encodeNewIngreso userId ingreso =
    object
        [ ( "userId", Encode.int userId )
        , ( "fecha", encodeMaybeDate ingreso.fecha )
        , ( "numeroTimbrado", Encode.string ingreso.numeroTimbrado )
        , ( "numeroDocumento", Encode.string ingreso.numeroDocumento )
        , ( "numeroIdentificacion", Encode.string ingreso.numeroIdentificacion )
        , ( "gravado", encodeMaybeInt ingreso.gravado )
        , ( "noGravado", encodeMaybeInt ingreso.noGravado )
        , ( "nombre", Encode.string ingreso.nombre )
        , ( "tipoDocumentoId", encodeMaybeIntString ingreso.tipoDocumento )
        , ( "condicionVentaId", encodeMaybeIntString ingreso.condicionVenta )
        , ( "tipoIdentificacionId", encodeMaybeIntString ingreso.tipoIdentificacion )
        , ( "tipoIngresoId", encodeMaybeIntString ingreso.tipoIngreso )
        ]


postIngreso : String -> LoginData -> Ingreso -> Cmd Msg
postIngreso apiUrl loginData ingreso =
    Common.post
        { url = apiUrl ++ "ingresos"
        , token = loginData.token
        , body = encodeNewIngreso loginData.userId ingreso |> Http.jsonBody
        , expect = Http.expectJson GotNewIngreso idDecoder
        }


update : Msg -> Model -> ( Model, Answer )
update msg model =
    let
        ingreso =
            model.ingreso
    in
    case msg of
        ChangePicker changeEvent ->
            case changeEvent of
                DateChanged date ->
                    let
                        newIngreso =
                            { ingreso | fecha = Just date }
                    in
                    ( { model
                        | ingreso = newIngreso
                        , dateText = Date.toIsoString date
                      }
                    , NoAnswer
                    )

                TextChanged text ->
                    ( model, NoAnswer )

                PickerChanged subMsg ->
                    -- internal stuff changed
                    -- call DatePicker.update
                    ( { model
                        | pickerModel =
                            model.pickerModel
                                |> DatePicker.update subMsg
                      }
                    , NoAnswer
                    )

        SetToday today ->
            let
                newIngreso =
                    { ingreso | fecha = Just today }
            in
            ( { model
                | pickerModel =
                    model.pickerModel
                        |> DatePicker.setToday today
                , dateText = Date.toIsoString today
                , ingreso = newIngreso
              }
            , NoAnswer
            )

        GotTiposDocumento result ->
            case result of
                Ok tiposDocumento ->
                    let
                        newIngreso =
                            { ingreso
                                | tipoDocumento =
                                    Maybe.map
                                        (\x -> String.fromInt x.id)
                                        (List.head tiposDocumento)
                            }
                    in
                    ( { model
                        | tiposDocumento = tiposDocumento
                        , ingreso = newIngreso
                      }
                    , NoAnswer
                    )

                Err err ->
                    ( model, NoAnswer )

        GotCondicionesVenta result ->
            case result of
                Ok condicionesVenta ->
                    let
                        newIngreso =
                            { ingreso
                                | condicionVenta =
                                    Maybe.map
                                        (\x -> String.fromInt x.id)
                                        (List.head condicionesVenta)
                            }
                    in
                    ( { model
                        | condicionesVenta = condicionesVenta
                        , ingreso = newIngreso
                      }
                    , NoAnswer
                    )

                Err err ->
                    ( model, NoAnswer )

        GotTiposIdentificacion result ->
            case result of
                Ok tiposIdentificacion ->
                    let
                        newIngreso =
                            { ingreso
                                | tipoIdentificacion =
                                    Maybe.map
                                        (\x -> String.fromInt x.id)
                                        (List.head tiposIdentificacion)
                            }
                    in
                    ( { model
                        | tiposIdentificacion = tiposIdentificacion
                        , ingreso = newIngreso
                      }
                    , NoAnswer
                    )

                Err err ->
                    ( model, NoAnswer )

        GotTiposIngreso result ->
            case result of
                Ok tiposIngreso ->
                    let
                        newIngreso =
                            { ingreso
                                | tipoIngreso =
                                    Maybe.map
                                        (\x -> String.fromInt x.id)
                                        (List.head tiposIngreso)
                            }
                    in
                    ( { model
                        | tiposIngreso = tiposIngreso
                        , ingreso = newIngreso
                      }
                    , NoAnswer
                    )

                Err err ->
                    ( model, NoAnswer )

        NumeroTimbradoChanged s ->
            let
                newIngreso =
                    { ingreso | numeroTimbrado = s }
            in
            ( { model | ingreso = newIngreso }, NoAnswer )

        NumeroDocumentoChanged s ->
            let
                newIngreso =
                    { ingreso | numeroDocumento = s }
            in
            ( { model | ingreso = newIngreso }, NoAnswer )

        NumeroIdentificacionChanged s ->
            let
                newIngreso =
                    { ingreso | numeroIdentificacion = s }
            in
            ( { model | ingreso = newIngreso }, NoAnswer )

        GravadoChanged s ->
            let
                newIngreso =
                    { ingreso | gravado = stringToMaybeInt s ingreso.gravado }
            in
            ( { model | ingreso = newIngreso }, NoAnswer )

        NoGravadoChanged s ->
            let
                newIngreso =
                    { ingreso | noGravado = stringToMaybeInt s ingreso.noGravado }
            in
            ( { model | ingreso = newIngreso }, NoAnswer )

        NombreChanged s ->
            let
                newIngreso =
                    { ingreso | nombre = s }
            in
            ( { model | ingreso = newIngreso }, NoAnswer )

        CancelNewIngreso ->
            ( model, Close )

        OkNewIngreso ->
            ( model
            , SendCmd <|
                postIngreso
                    model.apiUrl
                    model.loginData
                    model.ingreso
            )

        GotNewIngreso _ ->
            ( model, Close )

        TipoDocumentoChanged tipoDocumento ->
            let
                newIngreso =
                    { ingreso | tipoDocumento = tipoDocumento }
            in
            ( { model | ingreso = newIngreso }, NoAnswer )

        CondicionVentaChanged condicionVenta ->
            let
                newIngreso =
                    { ingreso | condicionVenta = condicionVenta }
            in
            ( { model | ingreso = newIngreso }, NoAnswer )

        TipoIdentificacionChanged tipoIdentificacion ->
            let
                newIngreso =
                    { ingreso | tipoIdentificacion = tipoIdentificacion }
            in
            ( { model | ingreso = newIngreso }, NoAnswer )

        TipoIngresoChanged tipoIngreso ->
            let
                newIngreso =
                    { ingreso | tipoIngreso = tipoIngreso }
            in
            ( { model | ingreso = newIngreso }, NoAnswer )

        NoAction ->
            ( model, NoAnswer )


dropdownOptions : IdNombres -> (Maybe String -> Msg) -> Dropdown.Options Msg
dropdownOptions values msg =
    let
        defaultOptions =
            Dropdown.defaultOptions msg
    in
    { defaultOptions
        | items =
            List.map
                (\v ->
                    { value = String.fromInt v.id
                    , text = v.nombre
                    , enabled = True
                    }
                )
                values
        , emptyItem = Just { value = "", text = "[Seleccione por favor]", enabled = True }
    }


view : Model -> Element Msg
view model =
    column [ spacing 7 ]
        [ Element.text "Ingreso"
        , row []
            [ el [ width (px 250) ] (Element.text "Fecha")
            , DatePicker.input [ Element.centerX, Element.centerY ]
                { onChange = ChangePicker
                , selected = model.ingreso.fecha
                , text = model.dateText
                , label = Input.labelHidden "Fecha"
                , placeholder = Nothing
                , settings =
                    let
                        settings =
                            DatePicker.defaultSettings
                    in
                    { settings | language = Just es }
                , model = model.pickerModel
                }
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Numero Timbrado")
            , Input.text []
                { text = model.ingreso.numeroTimbrado
                , label = Input.labelHidden "Numero Timbrado"
                , placeholder = Nothing
                , onChange = \s -> NumeroTimbradoChanged s
                }
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Numero Documento")
            , Input.text []
                { text = model.ingreso.numeroDocumento
                , label = Input.labelHidden "Numero Documento"
                , placeholder = Nothing
                , onChange = \s -> NumeroDocumentoChanged s
                }
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Numero Identificacion")
            , Input.text []
                { text = model.ingreso.numeroIdentificacion
                , label = Input.labelHidden "Numero Identificacion"
                , placeholder = Nothing
                , onChange = \s -> NumeroIdentificacionChanged s
                }
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Ingreso Gravado")
            , Input.text
                textAlignRight
                { text = maybeIntToString model.ingreso.gravado
                , label = Input.labelHidden "Ingreso Gravado"
                , placeholder = Nothing
                , onChange = \s -> GravadoChanged s
                }
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Ingreso No Gravado")
            , Input.text
                textAlignRight
                { text = maybeIntToString model.ingreso.noGravado
                , label = Input.labelHidden "Ingreso No Gravado"
                , placeholder = Nothing
                , onChange = \s -> NoGravadoChanged s
                }
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Nombre o Razon Social")
            , Input.text []
                { text = model.ingreso.nombre
                , label = Input.labelHidden "Nombre o Razon Social"
                , placeholder = Nothing
                , onChange = \s -> NombreChanged s
                }
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Tipo Documento")
            , el [ width (px 300) ] <|
                html <|
                    Dropdown.dropdown
                        (dropdownOptions model.tiposDocumento TipoDocumentoChanged)
                        []
                        model.ingreso.tipoDocumento
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Condicion Venta")
            , el [ width (px 300) ] <|
                html <|
                    Dropdown.dropdown
                        (dropdownOptions model.condicionesVenta CondicionVentaChanged)
                        []
                        model.ingreso.condicionVenta
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Tipo Identificacion")
            , el [ width (px 300) ] <|
                html <|
                    Dropdown.dropdown
                        (dropdownOptions model.tiposIdentificacion TipoIdentificacionChanged)
                        []
                        model.ingreso.tipoIdentificacion
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Tipo Ingreso")
            , el [ width (px 300) ] <|
                html <|
                    Dropdown.dropdown
                        (dropdownOptions model.tiposIngreso TipoIngresoChanged)
                        []
                        model.ingreso.tipoIngreso
            ]
        , row [ width fill ]
            [ Input.button [ alignLeft ]
                { onPress = Just CancelNewIngreso
                , label = Element.text "Cancelar"
                }
            , Input.button [ alignRight ]
                { onPress = Just OkNewIngreso
                , label = Element.text "Aceptar"
                }
            ]
        ]
