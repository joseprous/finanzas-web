module Egreso exposing (..)

import Browser
import Browser.Dom as Dom
import Browser.Navigation as Nav
import Common exposing (..)
import Date exposing (Date)
import DatePicker exposing (ChangeEvent(..))
import Dropdown
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Html exposing (Html)
import Html.Attributes
import Http exposing (..)
import Json.Decode exposing (Decoder, decodeString, field, float, int, list, map2, map3, map4, maybe, string)
import Json.Encode as Encode exposing (Value, object)
import Maybe.Extra
import Route exposing (Route, fromUrl)
import Task
import Url


type Answer
    = Close
    | SendCmd (Cmd Msg)
    | NoAnswer


type alias Egreso =
    { categoria : Maybe String
    , tipoDocumento : Maybe String
    , condicionVenta : Maybe String
    , tipoIdentificacion : Maybe String
    , tipoEgreso : Maybe String
    , clasificacionEgreso : Maybe String
    , fecha : Maybe Date
    , numeroTimbrado : String
    , numeroDocumento : String
    , numeroIdentificacion : String
    , iva10 : Maybe Int
    , iva5 : Maybe Int
    , exentas : Maybe Int
    , nombre : String
    , monto : Int
    }


type alias Model =
    { categorias : IdNombres
    , tiposDocumento : IdNombres
    , condicionesVenta : IdNombres
    , tiposIdentificacion : IdNombres
    , tiposEgreso : IdNombres
    , clasificacionesEgreso : IdNombres
    , egreso : Egreso
    , dateText : String
    , pickerModel : DatePicker.Model
    , apiUrl : String
    , loginData : LoginData
    }


type Msg
    = GotCategorias (Result Http.Error IdNombres)
    | GotTiposDocumento (Result Http.Error IdNombres)
    | GotCondicionesVenta (Result Http.Error IdNombres)
    | GotTiposIdentificacion (Result Http.Error IdNombres)
    | GotTiposEgreso (Result Http.Error IdNombres)
    | GotClasificacionesEgreso (Result Http.Error IdNombres)
    | NumeroTimbradoChanged String
    | NumeroDocumentoChanged String
    | NumeroIdentificacionChanged String
    | Iva10Changed String
    | Iva5Changed String
    | ExentasChanged String
    | NombreChanged String
    | ChangePicker ChangeEvent
    | SetToday Date
    | CancelNewEgreso
    | OkNewEgreso
    | GotNewEgreso (Result Http.Error Int)
    | EnterPressed Int
    | CategoriaChanged (Maybe String)
    | TipoDocumentoChanged (Maybe String)
    | CondicionVentaChanged (Maybe String)
    | TipoIdentificacionChanged (Maybe String)
    | TipoEgresoChanged (Maybe String)
    | ClasificacionEgresoChanged (Maybe String)
    | NoAction


initEgreso : Egreso
initEgreso =
    { categoria = Nothing
    , tipoDocumento = Nothing
    , condicionVenta = Nothing
    , tipoIdentificacion = Nothing
    , tipoEgreso = Nothing
    , clasificacionEgreso = Nothing
    , fecha = Nothing
    , numeroTimbrado = ""
    , numeroDocumento = ""
    , numeroIdentificacion = ""
    , iva10 = Nothing
    , iva5 = Nothing
    , exentas = Nothing
    , nombre = ""
    , monto = 0
    }


init : String -> LoginData -> Model
init apiUrl loginData =
    { categorias = []
    , tiposDocumento = []
    , condicionesVenta = []
    , tiposIdentificacion = []
    , tiposEgreso = []
    , clasificacionesEgreso = []
    , egreso = initEgreso
    , dateText = ""
    , pickerModel = DatePicker.init
    , apiUrl = apiUrl
    , loginData = loginData
    }


getCategorias : String -> String -> Cmd Msg
getCategorias apiUrl token =
    Common.get
        { url = apiUrl ++ "categorias"
        , token = token
        , expect = Http.expectJson GotCategorias idNombresDecoder
        }


getTiposDocumento : String -> String -> Cmd Msg
getTiposDocumento apiUrl token =
    Common.get
        { url = apiUrl ++ "tipos-documento-egreso"
        , token = token
        , expect = Http.expectJson GotTiposDocumento idNombresDecoder
        }


getCondicionesVenta : String -> String -> Cmd Msg
getCondicionesVenta apiUrl token =
    Common.get
        { url = apiUrl ++ "condiciones-venta"
        , token = token
        , expect = Http.expectJson GotCondicionesVenta idNombresDecoder
        }


getTiposIdentificacion : String -> String -> Cmd Msg
getTiposIdentificacion apiUrl token =
    Common.get
        { url = apiUrl ++ "tipos-identificacion"
        , token = token
        , expect = Http.expectJson GotTiposIdentificacion idNombresDecoder
        }


getTiposEgreso : String -> String -> Cmd Msg
getTiposEgreso apiUrl token =
    Common.get
        { url = apiUrl ++ "tipos-egreso"
        , token = token
        , expect = Http.expectJson GotTiposEgreso idNombresDecoder
        }


getClasificacionesEgreso : String -> String -> Cmd Msg
getClasificacionesEgreso apiUrl token =
    Common.get
        { url = apiUrl ++ "clasificaciones-egreso"
        , token = token
        , expect = Http.expectJson GotClasificacionesEgreso idNombresDecoder
        }


load : String -> String -> Cmd Msg
load apiUrl token =
    Cmd.batch
        [ getCategorias apiUrl token
        , getTiposDocumento apiUrl token
        , getCondicionesVenta apiUrl token
        , getTiposIdentificacion apiUrl token
        , getTiposEgreso apiUrl token
        , getClasificacionesEgreso apiUrl token
        , Task.perform SetToday Date.today
        ]


updateMonto : Egreso -> Egreso
updateMonto egreso =
    { egreso
        | monto =
            Maybe.withDefault 0 egreso.iva10
                + Maybe.withDefault 0 egreso.iva5
                + Maybe.withDefault 0 egreso.exentas
    }


encodeNewEgreso : Int -> Egreso -> Value
encodeNewEgreso userId egreso =
    object
        [ ( "userId", Encode.int userId )
        , ( "categoriaId", encodeMaybeIntString egreso.categoria )
        , ( "fecha", encodeMaybeDate egreso.fecha )
        , ( "numeroTimbrado", Encode.string egreso.numeroTimbrado )
        , ( "numeroDocumento", Encode.string egreso.numeroDocumento )
        , ( "numeroIdentificacion", Encode.string egreso.numeroIdentificacion )
        , ( "iva10", encodeMaybeInt egreso.iva10 )
        , ( "iva5", encodeMaybeInt egreso.iva5 )
        , ( "exentas", encodeMaybeInt egreso.exentas )
        , ( "nombre", Encode.string egreso.nombre )
        , ( "tipoDocumentoId", encodeMaybeIntString egreso.tipoDocumento )
        , ( "condicionVentaId", encodeMaybeIntString egreso.condicionVenta )
        , ( "tipoIdentificacionId", encodeMaybeIntString egreso.tipoIdentificacion )
        , ( "tipoEgresoId", encodeMaybeIntString egreso.tipoEgreso )
        , ( "clasificacionEgresoId", encodeMaybeIntString egreso.clasificacionEgreso )
        , ( "monto", Encode.int egreso.monto )
        ]


postEgreso : String -> LoginData -> Egreso -> Cmd Msg
postEgreso apiUrl loginData egreso =
    Common.post
        { url = apiUrl ++ "egresos"
        , token = loginData.token
        , body = encodeNewEgreso loginData.userId egreso |> Http.jsonBody
        , expect = Http.expectJson GotNewEgreso idDecoder
        }


update : Msg -> Model -> ( Model, Answer )
update msg model =
    let
        egreso =
            model.egreso
    in
    case msg of
        GotCategorias result ->
            case result of
                Ok categorias ->
                    let
                        newEgreso =
                            { egreso
                                | categoria =
                                    Maybe.map
                                        (\x -> String.fromInt x.id)
                                        (List.head categorias)
                            }
                    in
                    ( { model
                        | categorias = categorias
                        , egreso = newEgreso
                      }
                    , NoAnswer
                    )

                Err err ->
                    ( model, NoAnswer )

        GotTiposDocumento result ->
            case result of
                Ok tiposDocumento ->
                    let
                        newEgreso =
                            { egreso
                                | tipoDocumento =
                                    Maybe.map
                                        (\x -> String.fromInt x.id)
                                        (List.head tiposDocumento)
                            }
                    in
                    ( { model
                        | tiposDocumento = tiposDocumento
                        , egreso = newEgreso
                      }
                    , NoAnswer
                    )

                Err err ->
                    ( model, NoAnswer )

        GotCondicionesVenta result ->
            case result of
                Ok condicionesVenta ->
                    let
                        newEgreso =
                            { egreso
                                | condicionVenta =
                                    Maybe.map
                                        (\x -> String.fromInt x.id)
                                        (List.head condicionesVenta)
                            }
                    in
                    ( { model
                        | condicionesVenta = condicionesVenta
                        , egreso = newEgreso
                      }
                    , NoAnswer
                    )

                Err err ->
                    ( model, NoAnswer )

        GotTiposIdentificacion result ->
            case result of
                Ok tiposIdentificacion ->
                    let
                        newEgreso =
                            { egreso
                                | tipoIdentificacion =
                                    Maybe.map
                                        (\x -> String.fromInt x.id)
                                        (List.head tiposIdentificacion)
                            }
                    in
                    ( { model
                        | tiposIdentificacion = tiposIdentificacion
                        , egreso = newEgreso
                      }
                    , NoAnswer
                    )

                Err err ->
                    ( model, NoAnswer )

        GotTiposEgreso result ->
            case result of
                Ok tiposEgreso ->
                    let
                        newEgreso =
                            { egreso
                                | tipoEgreso =
                                    Maybe.map
                                        (\x -> String.fromInt x.id)
                                        (List.head tiposEgreso)
                            }
                    in
                    ( { model
                        | tiposEgreso = tiposEgreso
                        , egreso = newEgreso
                      }
                    , NoAnswer
                    )

                Err err ->
                    ( model, NoAnswer )

        GotClasificacionesEgreso result ->
            case result of
                Ok clasificacionesEgreso ->
                    let
                        newEgreso =
                            { egreso
                                | clasificacionEgreso =
                                    Maybe.map
                                        (\x -> String.fromInt x.id)
                                        (List.head clasificacionesEgreso)
                            }
                    in
                    ( { model
                        | clasificacionesEgreso = clasificacionesEgreso
                        , egreso = newEgreso
                      }
                    , NoAnswer
                    )

                Err err ->
                    ( model, NoAnswer )

        NumeroTimbradoChanged s ->
            let
                newEgreso =
                    { egreso | numeroTimbrado = s }
            in
            ( { model | egreso = newEgreso }, NoAnswer )

        NumeroDocumentoChanged s ->
            let
                newEgreso =
                    { egreso | numeroDocumento = s }
            in
            ( { model | egreso = newEgreso }, NoAnswer )

        NumeroIdentificacionChanged s ->
            let
                newEgreso =
                    { egreso | numeroIdentificacion = s }
            in
            ( { model | egreso = newEgreso }, NoAnswer )

        Iva10Changed s ->
            let
                newEgreso =
                    updateMonto
                        { egreso | iva10 = stringToMaybeInt s egreso.iva10 }
            in
            ( { model | egreso = newEgreso }, NoAnswer )

        Iva5Changed s ->
            let
                newEgreso =
                    updateMonto
                        { egreso | iva5 = stringToMaybeInt s egreso.iva5 }
            in
            ( { model | egreso = newEgreso }, NoAnswer )

        ExentasChanged s ->
            let
                newEgreso =
                    updateMonto
                        { egreso | exentas = stringToMaybeInt s egreso.exentas }
            in
            ( { model | egreso = newEgreso }, NoAnswer )

        NombreChanged s ->
            let
                newEgreso =
                    { egreso | nombre = s }
            in
            ( { model | egreso = newEgreso }, NoAnswer )

        ChangePicker changeEvent ->
            case changeEvent of
                DateChanged date ->
                    let
                        newEgreso =
                            { egreso | fecha = Just date }
                    in
                    ( { model
                        | egreso = newEgreso
                        , dateText = Date.toIsoString date
                      }
                    , NoAnswer
                    )

                TextChanged text ->
                    ( model, NoAnswer )

                PickerChanged subMsg ->
                    -- internal stuff changed
                    -- call DatePicker.update
                    ( { model
                        | pickerModel =
                            model.pickerModel
                                |> DatePicker.update subMsg
                      }
                    , NoAnswer
                    )

        SetToday today ->
            let
                newEgreso =
                    { egreso | fecha = Just today }
            in
            ( { model
                | pickerModel =
                    model.pickerModel
                        |> DatePicker.setToday today
                , dateText = Date.toIsoString today
                , egreso = newEgreso
              }
            , NoAnswer
            )

        CancelNewEgreso ->
            ( model, Close )

        OkNewEgreso ->
            ( model
            , SendCmd <|
                postEgreso
                    model.apiUrl
                    model.loginData
                    model.egreso
            )

        GotNewEgreso _ ->
            ( model, Close )

        EnterPressed n ->
            ( model
            , SendCmd <|
                Task.attempt (\_ -> NoAction)
                    (Dom.focus ("field" ++ String.fromInt (n + 1)))
            )

        CategoriaChanged categoria ->
            let
                newEgreso =
                    { egreso | categoria = categoria }
            in
            ( { model | egreso = newEgreso }, NoAnswer )

        TipoDocumentoChanged tipoDocumento ->
            let
                newEgreso =
                    { egreso | tipoDocumento = tipoDocumento }
            in
            ( { model | egreso = newEgreso }, NoAnswer )

        CondicionVentaChanged condicionVenta ->
            let
                newEgreso =
                    { egreso | condicionVenta = condicionVenta }
            in
            ( { model | egreso = newEgreso }, NoAnswer )

        TipoIdentificacionChanged tipoIdentificacion ->
            let
                newEgreso =
                    { egreso | tipoIdentificacion = tipoIdentificacion }
            in
            ( { model | egreso = newEgreso }, NoAnswer )

        TipoEgresoChanged tipoEgreso ->
            let
                newEgreso =
                    { egreso | tipoEgreso = tipoEgreso }
            in
            ( { model | egreso = newEgreso }, NoAnswer )

        ClasificacionEgresoChanged clasificacionEgreso ->
            let
                newEgreso =
                    { egreso | clasificacionEgreso = clasificacionEgreso }
            in
            ( { model | egreso = newEgreso }, NoAnswer )

        NoAction ->
            ( model, NoAnswer )


fieldAttr : Int -> List (Attribute Msg)
fieldAttr n =
    [ onEnter (EnterPressed n)
    , htmlAttribute <| Html.Attributes.id ("field" ++ String.fromInt n)
    ]


dropdownOptions : IdNombres -> (Maybe String -> Msg) -> Dropdown.Options Msg
dropdownOptions values msg =
    let
        defaultOptions =
            Dropdown.defaultOptions msg
    in
    { defaultOptions
        | items =
            List.map
                (\v ->
                    { value = String.fromInt v.id
                    , text = v.nombre
                    , enabled = True
                    }
                )
                values
        , emptyItem = Just { value = "", text = "[Seleccione por favor]", enabled = True }
    }


view : Model -> Element Msg
view model =
    column [ spacing 7 ]
        [ Element.text "Egreso"
        , row []
            [ el [ width (px 250) ] (Element.text "Categoria")
            , el [ width (px 300) ] <|
                html <|
                    Dropdown.dropdown
                        (dropdownOptions model.categorias CategoriaChanged)
                        []
                        model.egreso.categoria
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Fecha")
            , DatePicker.input [ Element.centerX, Element.centerY ]
                { onChange = ChangePicker
                , selected = model.egreso.fecha
                , text = model.dateText
                , label = Input.labelHidden "Fecha"
                , placeholder = Nothing
                , settings =
                    let
                        settings =
                            DatePicker.defaultSettings
                    in
                    { settings | language = Just es }
                , model = model.pickerModel
                }
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Numero Timbrado")
            , Input.text
                (fieldAttr 0)
                { text = model.egreso.numeroTimbrado
                , label = Input.labelHidden "Numero Timbrado"
                , placeholder = Nothing
                , onChange = \s -> NumeroTimbradoChanged s
                }
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Numero Documento")
            , Input.text
                (fieldAttr 1)
                { text = model.egreso.numeroDocumento
                , label = Input.labelHidden "Numero Documento"
                , placeholder = Nothing
                , onChange = \s -> NumeroDocumentoChanged s
                }
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Numero Identificacion")
            , Input.text
                (fieldAttr 2)
                { text = model.egreso.numeroIdentificacion
                , label = Input.labelHidden "Numero Identificacion"
                , placeholder = Nothing
                , onChange = \s -> NumeroIdentificacionChanged s
                }
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Iva 10%")
            , Input.text
                (fieldAttr 3 ++ textAlignRight)
                { text = maybeIntToString model.egreso.iva10
                , label = Input.labelHidden "Iva 10%"
                , placeholder = Nothing
                , onChange = \s -> Iva10Changed s
                }
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Iva 5%")
            , Input.text
                (fieldAttr 4 ++ textAlignRight)
                { text = maybeIntToString model.egreso.iva5
                , label = Input.labelHidden "Iva 5%"
                , placeholder = Nothing
                , onChange = \s -> Iva5Changed s
                }
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Exentas")
            , Input.text
                (fieldAttr 5 ++ textAlignRight)
                { text = maybeIntToString model.egreso.exentas
                , label = Input.labelHidden "Exentas"
                , placeholder = Nothing
                , onChange = \s -> ExentasChanged s
                }
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Nombre o Razon Social")
            , Input.text
                (fieldAttr 6)
                { text = model.egreso.nombre
                , label = Input.labelHidden "Nombre o Razon Social"
                , placeholder = Nothing
                , onChange = \s -> NombreChanged s
                }
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Tipo Documento")
            , el [ width (px 300) ] <|
                html <|
                    Dropdown.dropdown
                        (dropdownOptions model.tiposDocumento TipoDocumentoChanged)
                        []
                        model.egreso.tipoDocumento
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Condicion Venta")
            , el [ width (px 300) ] <|
                html <|
                    Dropdown.dropdown
                        (dropdownOptions model.condicionesVenta CondicionVentaChanged)
                        []
                        model.egreso.condicionVenta
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Tipo Identificacion")
            , el [ width (px 300) ] <|
                html <|
                    Dropdown.dropdown
                        (dropdownOptions model.tiposIdentificacion TipoIdentificacionChanged)
                        []
                        model.egreso.tipoIdentificacion
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Tipo Egreso")
            , el [ width (px 300) ] <|
                html <|
                    Dropdown.dropdown
                        (dropdownOptions model.tiposEgreso TipoEgresoChanged)
                        []
                        model.egreso.tipoEgreso
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Clasificacion Egreso")
            , el [ width (px 300) ] <|
                html <|
                    Dropdown.dropdown
                        (dropdownOptions model.clasificacionesEgreso ClasificacionEgresoChanged)
                        []
                        model.egreso.clasificacionEgreso
            ]
        , row [ width fill ]
            [ Input.button [ alignLeft ]
                { onPress = Just CancelNewEgreso
                , label = Element.text "Cancelar"
                }
            , Input.button [ alignRight ]
                { onPress = Just OkNewEgreso
                , label = Element.text "Aceptar"
                }
            ]
        ]
