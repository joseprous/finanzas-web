module Login exposing (..)

import Browser
import Browser.Navigation as Nav
import Common exposing (..)
import Date exposing (Date)
import DatePicker exposing (ChangeEvent(..))
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Html exposing (Html)
import Html.Attributes
import Http exposing (..)
import Json.Decode exposing (Decoder, decodeString, field, float, int, list, map2, map3, map4, maybe, string)
import Json.Encode as Encode exposing (Value, object)
import Maybe.Extra
import Route exposing (Route, fromUrl)
import Task
import Url


type Answer
    = LoggedIn LoginData
    | SendCmd (Cmd Msg)
    | NoAnswer


type alias Model =
    { user : String
    , password : String
    , apiUrl : String
    }


type Msg
    = OkLogin
    | UserChanged String
    | PasswordChanged String
    | GotLogin (Result Http.Error LoginData)
    | NoAction


init : String -> Model
init apiUrl =
    { user = ""
    , password = ""
    , apiUrl = apiUrl
    }


encodeLogin : Model -> Value
encodeLogin login =
    object
        [ ( "username", Encode.string login.user )
        , ( "password", Encode.string login.password )
        ]


loginDecoder : Decoder LoginData
loginDecoder =
    map2 LoginData
        (field "userId" int)
        (field "token" string)


postLogin : String -> Model -> Cmd Msg
postLogin apiUrl login =
    Http.post
        { url = apiUrl ++ "login"
        , body = encodeLogin login |> Http.jsonBody
        , expect = Http.expectJson GotLogin loginDecoder
        }


update : Msg -> Model -> ( Model, Answer )
update msg model =
    case msg of
        UserChanged s ->
            ( { model | user = s }, NoAnswer )

        PasswordChanged s ->
            ( { model | password = s }, NoAnswer )

        OkLogin ->
            ( model
            , SendCmd <|
                postLogin
                    model.apiUrl
                    model
            )

        GotLogin result ->
            case result of
                Ok data ->
                    ( model
                    , LoggedIn data
                    )

                Err err ->
                    ( model, NoAnswer )

        NoAction ->
            ( model, NoAnswer )


view : Model -> Element Msg
view model =
    column [ spacing 7 ]
        [ Element.text "Ingreso"
        , row []
            [ el [ width (px 250) ] (Element.text "Usuario")
            , Input.text []
                { text = model.user
                , label = Input.labelHidden "Usuario"
                , placeholder = Nothing
                , onChange = \s -> UserChanged s
                }
            ]
        , row []
            [ el [ width (px 250) ] (Element.text "Password")
            , Input.currentPassword []
                { text = model.password
                , label = Input.labelHidden "Password"
                , placeholder = Nothing
                , show = False
                , onChange = \s -> PasswordChanged s
                }
            ]
        , Input.button [ width fill ]
            { onPress = Just OkLogin
            , label = Element.text "Login"
            }
        ]
